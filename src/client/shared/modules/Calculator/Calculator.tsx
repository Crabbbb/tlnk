import * as React from "react";
import * as S from "./Calculator.style";
import { validOperations, rounder, saveCalculatorResult } from "../../helpers";
import { FC, useRef, useState } from "react";
import HistoryTable from "../HistoryTable/HistoryTable";
import { useEffect } from "react";

const Calculator: FC = () => {
  const [message, setMessage] = useState<string>("");
  const [result, setResult] = useState<number | null>(0);

  const [history, setHistory] = useState<Array<string>>([]);

  const operandARef: any = useRef(0);
  const operandBRef: any = useRef(0);

  useEffect(() => {
    const localstorageHistory = localStorage.getItem("calculatorHistory");
    const parsed: Array<string> = JSON.parse(localstorageHistory);
    if (!!localstorageHistory) setHistory(parsed);
  }, []);

  const handleMessage = (message: string) => {
    setResult(null);
    setMessage(message);
  };

  const generateMessage = (operand: string) => {
    const max = process.env?.REACT_APP_MAX_PRIME_VALUE || 9007199254740991;

    if (operand === "prime") return handleMessage(`Max operand: ${max}, min operand: 1`);
    if (operand === "/") return handleMessage(`Cannot be divided by zero`);

    handleMessage("Something went wrong");
  };

  const handleCalculation = (res: { operand: string; operation: any }) => {
    const operandA: number = operandARef.current.value;
    const operandB: number = operandBRef.current.value;

    const { operand, operation } = res;

    if (operandA && operandB) {
      setMessage("");
      const operationResult = operation(+operandA, +operandB);

      if (operationResult === 0 || !!operationResult) {
        const places = +process.env?.REACT_APP_DECIMAL_PLACES || 2;

        const roundedResult = rounder(operationResult, places);

        setResult(roundedResult);

        const resultObj = {
          x: operandA,
          y: operandB,
          result: roundedResult,
          operand: operand,
        };

        const latestHistory = saveCalculatorResult(resultObj);
        setHistory([...history, latestHistory]);
      } else {
        generateMessage(operation);
      }
    } else {
      handleMessage("Both values should be filled");
    }
  };

  const CalculatorOperandButtons: FC = () => {
    return (
      <>
        {validOperations.map((res, index) => (
          <button onClick={() => handleCalculation(res)} key={index}>
            {res.operand}
          </button>
        ))}
      </>
    );
  };

  return (
    <S.Container>
      <S.ResultContainer>
        <h3>{result && result}</h3>
        <h3>{message}</h3>
      </S.ResultContainer>

      <S.InputContainer>
        <input type="number" ref={operandARef} placeholder={"Value A"} />
        <input type="number" ref={operandBRef} placeholder={"Value B"} />
      </S.InputContainer>

      <S.ButtonsContainer>
        <CalculatorOperandButtons />
      </S.ButtonsContainer>

      <HistoryTable history={history} />
    </S.Container>
  );
};

export default Calculator;
