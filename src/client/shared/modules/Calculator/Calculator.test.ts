import {
  handleDivide,
  handleDivisionRemainder,
  handlePrime,
  handleSum,
} from "../../helpers";

let files: any[] = [];

describe("Summary", () => {
  files = [
    [1, 1, 2],
    [299.44, 505.01, 804.45],
    [2, 1, 3],
  ];

  test.each(files)(
    "Add numbers using sum method",
    (x: number, y: number, expected: number) => {
      expect(handleSum(x, y)).toBe(expected);
    }
  );
});

describe("Divide", () => {
  files = [
    [100, 20, 5],
    [202.55, 33.54, 6.0345],
    [2, 1, 2],
  ];

  test.each(files)(
    "Divide numbers using divide method",
    (x: number, y: number, expected: number) => {
      expect(handleDivide(x, y)).toBeCloseTo(expected, 2);
    }
  );
});

describe("Division reminder", () => {
  files = [
    [17, 5, 2],
    [202.55, 33.54, 1.31],
    [2, 1, 0],
  ];

  test.each(files)(
    "Divide numbers to get division reminder",
    (x: number, y: number, expected: number) => {
      expect(handleDivisionRemainder(x, y)).toBeCloseTo(expected, 2);
    }
  );
});

describe("Prime number", () => {
  files = [
    [18, 1, 17],
    [23523523, 1, 23523473],
    [6666617, 1, 6666617],
    [5, 235235, 235231],
    [4, 4, 0],
    [1, 4, 3],
    [4, 4, 0],
    [0, 0, null],
    [-125125, 12512512, null],
    [1, 1, 0],
    [124, 124124124124124, 124124124124061],
    // [479001599, 1, 479001599],
  ];

  test.each(files)("Get prime number between values", (x, y, expected) => {
    expect(handlePrime(x, y)).toEqual(expected);
  });
});
