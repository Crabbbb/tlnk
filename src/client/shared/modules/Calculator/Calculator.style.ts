import styled from "styled-components";

export const Container = styled.div`
  margin: auto;
  padding: 1rem;
  max-width: 35rem;
  border-radius: 0.375rem;
  border: 1px solid rgba(0, 0, 0, 0.41);
  box-shadow: rgb(0 0 0 / 7%) 0px 0px 2px, rgb(0 0 0 / 5%) 0px 3px 6px;
`;

export const ResultContainer = styled.div`
  position: relative;
  margin: 0 0 1rem 0;
  background-color: ${(props) => props.theme.brandColor};
  border-radius: 0.175rem;
  display: flex;
  align-items: center;
  min-height: 2rem;
  padding: 0.25rem 0.5rem;

  h3 {
    font-size: 2rem;
    margin: 0;
  }
`;

export const InputContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;

  input {
    width: 49%;
    height: 2rem;
    border: 1px solid rgba(0, 0, 0, 0.18);
    border-radius: 0.175rem;
    padding: 0 0.5rem;
  }
`;

export const ButtonsContainer = styled.div`
  position: relative;
  margin: 1rem 0;
  display: flex;
  justify-content: space-between;

  button {
    width: calc((100% / 4) - 0.25rem);
    margin: 0 0 0.25rem 0;
    height: 2.5rem;
    cursor: pointer;
    background-color: ${(props) => props.theme.brandButtonColor};
    border: 1px solid rgba(0, 0, 0, 0.18);
    border-radius: 0.175rem;
    transition: background-color 0.15s ease-in-out;
    &:hover {
      background-color: ${(props) => props.theme.brandButtonHoverColor};
    }
  }
`;
