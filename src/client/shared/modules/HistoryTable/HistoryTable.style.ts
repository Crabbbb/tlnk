import styled from "styled-components";

export const Container = styled.div`
  position: relative;
  height: 25rem;
  margin: 1rem 0 0 0;
`;

export const TableContainer = styled.div`
  position: relative;
  height: 21.5rem;
  overflow-y: auto;
  margin: 1rem 0 0 0;

  display: flex;
  flex-direction: column;
`;

export const RemoveHistoryButton = styled.button`
  position: relative;
  margin: 1rem 0 0 0;
  height: 2.5rem;
  width: 100%;
  padding: 0.5rem 0.25rem;
  font-size: 0.95rem;
  bottom: 0;
  cursor: pointer;
  border: 1px solid rgba(0, 0, 0, 0.18);
  border-radius: 0.175rem;
  background-color: ${(props) => props.theme.brandButtonColor};
  transition: background-color 0.15s ease-in-out;
  &:hover {
    background-color: ${(props) => props.theme.brandButtonHoverColor};
  }
`;

export const ResultField = styled.div`
  display: flex;
  flex-direction: row;
  padding: 1rem 0;

  &:nth-child(odd) {
    background-color: ${(props) => props.theme.brandColor};
    border-bottom: 1px solid rgba(0, 0, 0, 0.18);
    border-top: 1px solid rgba(0, 0, 0, 0.18);
  }

  p {
    margin: 0;
    word-break: break-all;
  }

  span {
    font-size: 0.9rem;
    margin: 0 0.5rem 0 0;
    font-weight: 700;
  }
`;
