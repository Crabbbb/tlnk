import * as React from "react";
import * as S from "./HistoryTable.style";
import { FC } from "react";
import { setLocalVariables } from "../../helpers";
import { removeLocalVariable } from "../../helpers/localVariables";

type HistoryTable = {
  history: Array<string>;
};

const HistoryTable: FC<HistoryTable> = ({ history }) => {
  setLocalVariables(history);

  const handleRemoveHistory = () => {
    localStorage.removeItem("calculatorHistory");
    removeLocalVariable();
    window.location.reload();
  };

  return (
    <S.Container>
      <S.TableContainer>
        {history.map((historyCalculation, index) => (
          <S.ResultField key={index}>
            <span>{`${index + 1})`}</span>
            <p>{historyCalculation}</p>
          </S.ResultField>
        ))}
      </S.TableContainer>
      <S.RemoveHistoryButton onClick={handleRemoveHistory}>
        Remove history
      </S.RemoveHistoryButton>
    </S.Container>
  );
};

export default HistoryTable;
