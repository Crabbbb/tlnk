import * as React from "react";
import { FC } from "react";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import routes from "./routes";
import "./App.scss";
import { getLocalVariable } from "./helpers";
import { theme } from "./theme";

type AppProps = {};

window.addEventListener("beforeunload", () => {
  const data = getLocalVariable();
  if (data) localStorage.setItem("calculatorHistory", JSON.stringify(data));
});

const routeComponent = routes().map<any>(
  ({ path, exact, component: Component }) => (
    <Route
      key={path}
      path={path}
      exact={exact}
      render={(props: any) => <Component {...props} />}
    />
  )
);

const App: FC<AppProps> = () => {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Switch>{routeComponent}</Switch>
      </Router>
    </ThemeProvider>
  );
};

export default App;
