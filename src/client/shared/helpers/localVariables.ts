const historyVariable = "__CALC_HISTORY__";

export const setLocalVariables = (data: Array<string>) =>
  ((window as any)[historyVariable] = data);

export const getLocalVariable = () => (window as any)[historyVariable];

export const removeLocalVariable = () =>
  delete (window as any)[historyVariable];
