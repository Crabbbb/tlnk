const isPrime = (X: number): number => {
  if (X === 1) return 0;

  // If X is divisible by 2 then we avoid (all even numbers)
  if (X % 2 == 0) return 2;
  if (X % 3 == 0) return 3;

  // No need to go past the square root of our number - we reduce loop(load) time
  const stop = Math.sqrt(X);

  // We loop through sqrt value by adding 2 and 4 to get odd numbers
  let di = 2;
  for (let i = 5; i <= stop; i += di, di = 6 - di) {
    // If remainder is 0 then return value with which it is divided
    if (X % i === 0) return i;
  }

  // It is prime if the value is the same it came from
  return X;
};

export const handlePrime = (
  operandA: number,
  operandB: number
): number | null => {
  const max = Math.max(operandA, operandB);
  const min = Math.min(operandA, operandB);

  const maxAllowed =
    +process.env?.REACT_APP_MAX_PRIME_VALUE || 9007199254740991;

  if (max > maxAllowed || min <= 0 || max % 1 !== 0 || min % 1 !== 0) {
    return null;
  }

  if (max === min) {
    const primeNumber = isPrime(max);
    if (primeNumber === max) return max;
  } else {
    for (let step = max; step >= min; step--) {
      const primeNumber = isPrime(step);
      if (primeNumber === step) return step;
    }
  }

  // Zero - indicator for not prime value
  return 0;
};
