import {
  handleSum,
  handleDivide,
  handleDivisionRemainder,
  handlePrime,
} from "../helpers";

export const validOperations: Array<{ operand: string; operation: any }> = [
  {
    operand: "+",
    operation: (x: number, y: number) => handleSum(x, y),
  },
  {
    operand: "/",
    operation: (x: number, y: number) => handleDivide(x, y),
  },
  {
    operand: "%",
    operation: (x: number, y: number) => handleDivisionRemainder(x, y),
  },
  {
    operand: "prime",
    operation: (x: number, y: number) => handlePrime(x, y),
  },
];
