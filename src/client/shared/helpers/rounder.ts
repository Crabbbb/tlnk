export const rounder = (x: number, places: number = 3): number => {
  if (x % 1 === 0) return x;
  const rounder = Math.pow(10, places);
  return Math.floor(x * rounder) / rounder;
};
