type saveCalculatorResultProps = {
  x: number;
  y: number;
  result: number;
  operand: string;
};

const handleResult = (operand: string, result: number) => {
  return {
    prime: !result ? "Prime not found between numbers" : result,
    "%": result,
    "+": result,
    "/": result,
  }[operand];
};

export const saveCalculatorResult = ({
  x,
  y,
  result,
  operand,
}: saveCalculatorResultProps) => {
  return `${x} ${operand} ${y} = ${handleResult(operand, result)}`;
};
