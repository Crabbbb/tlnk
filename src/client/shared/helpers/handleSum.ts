export const handleSum = (operandA: number, operandB: number): number => {
  return operandA + operandB;
};
