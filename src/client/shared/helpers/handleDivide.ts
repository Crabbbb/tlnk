export const handleDivide = (
  operandA: number,
  operandB: number
): number | null => {
  const result = operandA / operandB;
  return isFinite(result) ? result : null;
};
