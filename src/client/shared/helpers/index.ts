export { validOperations } from "./validOperations";
export { handleSum } from "./handleSum";
export { handleDivide } from "./handleDivide";
export { handlePrime } from "./handlePrime";
export { handleDivisionRemainder } from "./handledDivisionRemainder";
export { saveCalculatorResult } from "./saveCalculatorResult";
export { rounder } from "./rounder";
export { setLocalVariables, getLocalVariable } from "./localVariables";
