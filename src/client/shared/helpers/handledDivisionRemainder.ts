export const handleDivisionRemainder = (
  operandA: number,
  operandB: number
): number => {
  return operandA % operandB;
};
