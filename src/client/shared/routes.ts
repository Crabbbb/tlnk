import { FC } from "react";
import Calculator from "./modules/Calculator/Calculator";

const makeRoute = (path: string, component: FC, exact: boolean) => {
  return {
    path: path,
    exact: exact,
    component: component,
  };
};

const routes = () => {
  const cities = makeRoute(`/`, Calculator, true);
  return [cities];
};

export default routes;
