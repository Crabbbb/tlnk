export const theme = {
  // Main
  brandColor: "#F5F5F5",

  // Buttons
  brandButtonColor: "#CFCFCF",
  brandButtonHoverColor: "rgba(215,215,215,0.75)",
};
